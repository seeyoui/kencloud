/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysLogin;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seeyoui.kensite.base.controller.BaseController;
import com.seeyoui.kensite.base.domain.TreeJson;
import com.seeyoui.kensite.system.sysMenu.service.SysMenuService;
import com.seeyoui.kensite.system.sysUser.domain.SysUser;
import com.seeyoui.kensite.system.sysUser.service.SysUserService;
import com.seeyoui.kensite.utils.MD5;
import com.seeyoui.kensite.utils.StringUtils;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping(value = "sysLogin")
public class SysLoginController extends BaseController {
	
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysMenuService sysMenuService;
	
	/**
	 * 判断用户登录
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> login(HttpSession session,
		HttpServletResponse response, HttpServletRequest request,
		ModelMap modelMap, String userName, String passWord) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", false);
		SysUser sysUser = sysUserService.findByUserName(userName);
		if(sysUser != null && StringUtils.isNoneBlank(sysUser.getPassword())){
			if(MD5.md5(userName + passWord).equals(sysUser.getPassword())) {
				List<TreeJson> menuList = sysMenuService.findTree(sysUser);
				System.err.println("system      sessionId=" + request.getSession().getId());
				result.put("currentUserName", userName);
				result.put("currentUser", JSONObject.fromObject(sysUser));
				result.put("menuList", JSONArray.fromObject(menuList));
				result.put("success", true);
			} else {
				result.put("message", "密码错误");
			}
		} else {
			result.put("message", "用户名不存在");
		}
		return result;
	}
	
	/**
	 * 判断用户登录
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/logout", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> logout(HttpSession session,
		HttpServletResponse response, HttpServletRequest request,
		ModelMap modelMap) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		request.getSession().setAttribute("currentUserName", null);
		request.getSession().setAttribute("currentUser", null);
		request.getSession().setAttribute("menuList", null);
		System.err.println("system      sessionId=" + request.getSession().getId());
		result.put("success", true);
		return result;
	}
}