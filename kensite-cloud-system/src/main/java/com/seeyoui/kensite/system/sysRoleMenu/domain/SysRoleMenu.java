/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysRoleMenu.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.seeyoui.kensite.base.domain.BaseEntity;
import com.seeyoui.kensite.utils.GeneratorUUID;
import com.seeyoui.kensite.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="sys_role_menu")
public class SysRoleMenu extends BaseEntity<SysRoleMenu> {
	private static final long serialVersionUID = 5454155825314635342L;

	@Id
	@Column(name="id")
	private String id;
	@Column(name="role_id")
	private String roleId;
	@Column(name="menu_id")
	private String menuId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleId() {
		return this.roleId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenuId() {
		return this.menuId;
	}
	
	public void preInsert() {
		if(StringUtils.isBlank(this.id)) {
			this.id = GeneratorUUID.getId();
		}
	}
}