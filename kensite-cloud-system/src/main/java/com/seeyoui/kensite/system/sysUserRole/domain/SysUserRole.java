/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysUserRole.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.seeyoui.kensite.base.domain.BaseEntity;
import com.seeyoui.kensite.utils.GeneratorUUID;
import com.seeyoui.kensite.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="sys_user_role")
public class SysUserRole extends BaseEntity<SysUserRole> {
	private static final long serialVersionUID = 5454155825314635342L;
	
	@Id
	@Column(name="id")
	private String id;
	@Column(name="user_id")
	private String userId;
	@Column(name="role_id")
	private String roleId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleId() {
		return this.roleId;
	}
	
	public void preInsert() {
		if(StringUtils.isBlank(this.id)) {
			this.id = GeneratorUUID.getId();
		}
	}
}