/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysRole.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.seeyoui.kensite.base.domain.BaseEntity;
import com.seeyoui.kensite.utils.GeneratorUUID;
import com.seeyoui.kensite.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="sys_role")
public class SysRole extends BaseEntity<SysRole> {
	private static final long serialVersionUID = 5454155825314635342L;
	
	@Id
	@Column(name="id")
	private String id;
	@Column(name="name")
	private String name;
	@Column(name="shiro")
	private String shiro;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setShiro(String shiro) {
		this.shiro = shiro;
	}

	public String getShiro() {
		return this.shiro;
	}
	
	public void preInsert() {
		if(StringUtils.isBlank(this.id)) {
			this.id = GeneratorUUID.getId();
		}
	}
}