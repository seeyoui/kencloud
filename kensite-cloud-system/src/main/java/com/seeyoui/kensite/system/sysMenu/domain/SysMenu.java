/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysMenu.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.seeyoui.kensite.base.domain.BaseEntity;
import com.seeyoui.kensite.utils.GeneratorUUID;
import com.seeyoui.kensite.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="sys_menu")
public class SysMenu extends BaseEntity<SysMenu> {
	private static final long serialVersionUID = 5454155825314635342L;
	
	@Id
	@Column(name="id")
	private String id;
	@Column(name="parent_id")
	private String parentId;
	@Column(name="name")
	private String name;
	@Column(name="url")
	private String url;
	@Column(name="sequence")
	private Long sequence;
	@Column(name="icon")
	private String icon;
	@Column(name="target")
	private String target;
	@Column(name="state")
	private String state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId() {
		return this.parentId;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return this.url;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	
	public Long getSequence() {
		return this.sequence;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getIcon() {
		return this.icon;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	public String getTarget() {
		return this.target;
	}

	public String getState() {
		return "closed";
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public void preInsert() {
		if(StringUtils.isBlank(this.id)) {
			this.id = GeneratorUUID.getId();
		}
	}
}