/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysRole.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.seeyoui.kensite.exception.CRUDException;
import com.seeyoui.kensite.system.sysRole.domain.SysRole;
import com.seeyoui.kensite.system.sysUser.domain.SysUser;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@Mapper
public interface SysRoleMapper {

	/**
	 * 根据ID查询单条数据
	 * @param id
	 * @return
	 */
	public SysRole findOne(String id);
	
	/**
	 * 查询数据集合
	 * @param sysRole
	 * @return
	 */
	public List<SysRole> findList(SysRole sysRole);
	
	/**
	 * 查询数据集合
	 * @param sysRole
	 * @return
	 */
	public List<SysRole> findAll(SysRole sysRole);
	
	/**
	 * 查询用户权限集合
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<SysRole> findSysUserList(SysUser sysUser);
	
	/**
	 * 查询数据总数
	 * @param userinfo
	 * @return
	 */
	public int findTotal(SysRole sysRole);
	
	/**
	 * 数据新增
	 * @param sysRole
	 */
	public void save(SysRole sysRole);
	
	/**
	 * 数据修改
	 * @param sysRole
	 */
	public void update(SysRole sysRole);
	
	/**
	 * 数据删除
	 * @param listId
	 */
	public void delete(List<String> listId);
}