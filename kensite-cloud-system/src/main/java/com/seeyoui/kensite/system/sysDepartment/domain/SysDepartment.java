/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysDepartment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.seeyoui.kensite.base.domain.BaseEntity;
import com.seeyoui.kensite.utils.GeneratorUUID;
import com.seeyoui.kensite.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="sys_department")
public class SysDepartment extends BaseEntity<SysDepartment> {
	
	private static final long serialVersionUID = 8393860158312768562L;
	@Id
	@Column(name="id")
	private String id;
	@Column(name="parent_id")
	private String parentId;
	@Range(min = 1, max = 100)
	@Column(name="sequence")
	private Long sequence;
	@Column(name="name")
	private String name;
	@Length(min = 6, max = 8)
	@Column(name="code")
	private String code;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId() {
		return this.parentId;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	
	public Long getSequence() {
		return this.sequence;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public void preInsert() {
		if(StringUtils.isBlank(this.id)) {
			this.id = GeneratorUUID.getId();
		}
	}
}