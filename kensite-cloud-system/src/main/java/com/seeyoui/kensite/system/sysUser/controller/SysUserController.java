/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysUser.controller;
 
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.seeyoui.kensite.base.controller.BaseController;
import com.seeyoui.kensite.base.domain.EasyUIDataGrid;
import com.seeyoui.kensite.constants.StringConstant;
import com.seeyoui.kensite.system.sysUser.domain.SysUser;
import com.seeyoui.kensite.system.sysUser.service.SysUserService;
import com.seeyoui.kensite.utils.MD5;
import com.seeyoui.kensite.utils.StringUtils;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping(value = "sysUser")
public class SysUserController extends BaseController {
	
	@Autowired
	private SysUserService sysUserService;
	
	/**
	 * 展示列表页面
	 * @param modelMap
	 * @param module
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{page}")
	public ModelAndView page(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, @PathVariable String page) throws Exception {
		return new ModelAndView("system/sysUser/"+page, modelMap);
	}
	
	/**
	 * 获取列表展示数据
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list/data", method=RequestMethod.POST)
	@ResponseBody
	public Object listData(HttpSession session,
		HttpServletResponse response, HttpServletRequest request,
		ModelMap modelMap, SysUser sysUser) throws Exception{
		List<SysUser> sysUserList = sysUserService.findList(sysUser);
		int total = sysUserService.findTotal(sysUser);
		EasyUIDataGrid eudg = new EasyUIDataGrid();
		eudg.setRows(sysUserList);
		eudg.setTotal(String.valueOf(total));
		return eudg;
	}
	
	/**
	 * 保存新增的数据
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save", method=RequestMethod.POST)
	@ResponseBody
	public Object save(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUser sysUser) throws Exception{
		String resultInfo = sysUserService.save(sysUser);
		modelMap.put("message", resultInfo);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 保存修改的数据
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method=RequestMethod.POST)
	@ResponseBody
	public Object update(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUser sysUser) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		if (!beanValidator(modelMap, sysUser)){
			result.put("success", StringConstant.FALSE);
			return result;
		}
		sysUserService.update(sysUser);
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 修改用户密码
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePassword", method=RequestMethod.POST)
	@ResponseBody
	public Object updatePassword(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUser sysUser) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		if (!beanValidator(modelMap, sysUser)){
			result.put("success", StringConstant.FALSE);
			return result;
		}
		sysUser.setPassword(MD5.md5(sysUser.getUserName()+sysUser.getPassword()));
		sysUserService.updatePassword(sysUser);
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 修改账号状态
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateState", method=RequestMethod.POST)
	@ResponseBody
	public Object updateState(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUser sysUser) throws Exception{
		sysUserService.updateState(sysUser);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 初始化密码
	 * @param modelMap
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/initPassword", method=RequestMethod.POST)
	@ResponseBody
	public Object initPassword(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUser sysUser) throws Exception{
		sysUser = sysUserService.findOne(sysUser.getId());
		sysUser.setPassword(MD5.md5(sysUser.getUserName()+StringConstant.INIT_PASSWORD));
		sysUserService.updatePassword(sysUser);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 删除数据库
	 * @param modelMap
	 * @param sysUserId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method=RequestMethod.POST)
	@ResponseBody
	public Object delete(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, String id) throws Exception {
		List<String> listId = Arrays.asList(id.split(","));
		sysUserService.delete(listId);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 验证用户名是否已被占用
	 * @param modelMap
	 * @param sysUserId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/validateUserName")
	@ResponseBody
	public Object validateUserName(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, String userName) throws Exception {
		SysUser sysUserResult = sysUserService.findByUserName(userName);
		Map<String, Object> result = new HashMap<String, Object>();
		if(sysUserResult != null 
			&& (sysUserResult.getId() != null && !sysUserResult.getId().equals(""))){
			result.put("success", StringConstant.FALSE);
			return result;
		}
		result.put("success", StringConstant.TRUE);
		return result;
	}
	
	/**
	 * 验证用户名是否已被占用
	 * @param modelMap
	 * @param sysUserId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/validatePassWord")
	@ResponseBody
	public Object validatePassWord(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, String userName, String passWord) throws Exception {
		SysUser sysUserResult = sysUserService.findByUserName(userName);
		if(sysUserResult != null 
			&& (sysUserResult.getId() != null && !sysUserResult.getId().equals(""))){
			String newPassWord = MD5.md5(userName+passWord);
			if(newPassWord.equals(sysUserResult.getPassword())) {
				return true;
			}
		}
		return false;
	}
}