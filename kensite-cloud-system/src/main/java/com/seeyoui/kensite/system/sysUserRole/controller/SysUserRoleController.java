/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysUserRole.controller;  
 
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seeyoui.kensite.base.controller.BaseController;
import com.seeyoui.kensite.base.domain.TreeJson;
import com.seeyoui.kensite.constants.StringConstant;
import com.seeyoui.kensite.system.sysUserRole.domain.SysUserRole;
import com.seeyoui.kensite.system.sysUserRole.service.SysUserRoleService;
import com.seeyoui.kensite.utils.RequestResponseUtil;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping(value = "sysUserRole")
public class SysUserRoleController extends BaseController {
	
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	/**
	 * 获取列表展示数据
	 * @param modelMap
	 * @param sysUserRole
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tree", method=RequestMethod.POST)
	@ResponseBody
	public Object getTreeData(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUserRole sysUserRole) throws Exception{
		List<TreeJson> tList = sysUserRoleService.tree(sysUserRole);
		return tList;
	}
	
	/**
	 * 保存新增的数据
	 * @param modelMap
	 * @param sysUserRole
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save", method=RequestMethod.POST)
	@ResponseBody
	public String saveUserRole(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, SysUserRole sysUserRole) throws Exception{
		if (!beanValidator(modelMap, sysUserRole)){
			RequestResponseUtil.putResponseStr(session, response, request, modelMap, StringConstant.FALSE);
			return null;
		}
		sysUserRoleService.save(sysUserRole);
		RequestResponseUtil.putResponseStr(session, response, request, modelMap, StringConstant.TRUE);
		return null;
	}
}