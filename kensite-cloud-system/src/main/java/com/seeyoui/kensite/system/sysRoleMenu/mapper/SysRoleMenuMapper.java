/*
 * Powered By cuichen
 * Since 2014 - 2015
 */
package com.seeyoui.kensite.system.sysRoleMenu.mapper;  

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.seeyoui.kensite.exception.CRUDException;
import com.seeyoui.kensite.system.sysMenu.domain.SysMenu;
import com.seeyoui.kensite.system.sysRoleMenu.domain.SysRoleMenu;

/**
 * @author cuichen
 * @version 1.0
 * @since 1.0
 */
@Mapper
public interface SysRoleMenuMapper {

	/**
	 * 查询数据TREE
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<SysMenu> tree(SysRoleMenu sysRoleMenu);
	
	/**
	 * 数据新增
	 * @param sysRoleMenu
	 */
	public void save(SysRoleMenu sysRoleMenu);
	
	/**
	 * 数据删除
	 * @param listId
	 */
	public void delete(String roleid);
}