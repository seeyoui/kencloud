package com.seeyoui.kensite.system.sysUser.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.seeyoui.kensite.system.sysUser.domain.SysUser;

public interface SysUserRepository extends JpaRepository<SysUser,String> {

	/**
	 * 根据账号查询单条数据
	 * @param userName
	 * @return
	 */
	public SysUser findByUserName(String userName);
}