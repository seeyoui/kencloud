/*
Navicat MySQL Data Transfer

Source Server         : kensite
Source Server Version : 50517
Source Host           : localhost:3306
Source Database       : kencloud

Target Server Type    : MYSQL
Target Server Version : 50517
File Encoding         : 65001

Date: 2019-06-04 15:33:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  `ID` char(32) NOT NULL COMMENT '主键',
  `PARENT_ID` char(32) DEFAULT NULL COMMENT '外键',
  `SEQUENCE` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `NAME` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `CODE` varchar(100) DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='部门';

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('00000000000000000000000000000000', 'ffffffffffffffffffffffffffffffff', '0', '系统部门', 'dept');
INSERT INTO `sys_department` VALUES ('dad8faf2f3eb4d279ef04decb91ca4c0', '00000000000000000000000000000000', '0', '超级系统管理员', '001');
INSERT INTO `sys_department` VALUES ('e51ba1c688204117bd08b8ce2393e756', '00000000000000000000000000000000', '1', '研发部', 'YFB001');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `ID` char(32) NOT NULL COMMENT '主键',
  `VALUE` varchar(50) DEFAULT NULL COMMENT '数据值',
  `LABEL` varchar(50) DEFAULT NULL COMMENT '标签名',
  `CATEGORY` varchar(50) DEFAULT NULL COMMENT '分类',
  `DESCRIPTION` varchar(50) DEFAULT NULL COMMENT '描述',
  `SEQUENCE` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `PARENT_ID` char(32) DEFAULT NULL COMMENT '父主键',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='系统字典';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('006dd9700c6343a996fffdb837d4df02', 'sort', '排序方式', 'root', '排序方式字典', '2', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('0469a2108e854bbc83d5afa7b3367676', 'yes_no', '是否', 'root', '是否字典', '1', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('046e259674b2451c8790175079479c67', 'act_type', '流程分类', 'root', null, '10', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('05da54f04ce0408e89c839402f83b3f8', 'yes_no_hidden', '表单字段编辑权限', 'root', '表单是否编辑或隐藏', '2', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('069c81108d6c4fa4a8ea3f60cd6da37f', 'H', '隐藏', 'yes_no_hidden', '隐藏', '15', '05da54f04ce0408e89c839402f83b3f8');
INSERT INTO `sys_dict` VALUES ('0b36e6e0de4d40bea6bb078a38bbed22', '<=', '小于等于', 'query', '排序方式小于等于', '15', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('0d71cae660dd4c76912a946cc83326ba', 'quartz_category', '任务类别', 'root', '定时任务计划', '8', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('105a550d0fa54d7fa3abcc8cbf5371a3', 'tel', '电话', 'valid', '电话', '29', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('1125e32a6aa442e881d899574ed29c9f', 'Y', '是', 'yes_no', '是', '0', '0469a2108e854bbc83d5afa7b3367676');
INSERT INTO `sys_dict` VALUES ('11a156a7683241d68f8d07b3bfa9a213', 'avg', '平均值', 'oracle_operation', null, '10', '4f45bf65739c4268b580198001e4265b');
INSERT INTO `sys_dict` VALUES ('13956f4e6bca40a2a45670e9593904ba', 'DATETIME', 'DATETIME', 'jdbcType', 'DATETIME', '15', '838ebfaff94a42c5abd718e811f4da5f');
INSERT INTO `sys_dict` VALUES ('1631194214b64ae991366cdb01cab0af', 'M', '男', 'sex', '性别', '1', 'f5814ca746a145bc844669a70d984a9c');
INSERT INTO `sys_dict` VALUES ('19b950c4b2754fe4a050f24a7194789a', 'bo', 'BO视图', 'view_category', '', '5', 'a32de4aa80e747a598070bb030df3abb');
INSERT INTO `sys_dict` VALUES ('1fc867dcf4e4404ca8127a85f6d215b2', 'textbox', '单行', 'columnCategory', '单行输入框', '5', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('238c813e6157415bad169b28ec939346', '<', '小于', 'query', '排序方式小于', '5', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('25f2253e48c142b9bff112b0500c9bca', 'url', 'URL', 'valid', 'URL格式校验', '3', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('276695a7bf264d409f95290992a13044', 'radiobox', '单选', 'columnCategory', '单选按钮组', '20', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('27b2f554dc474ddc81840b53dab6e4e0', 'Y', '是', 'yes_no_hidden', '是', '5', '05da54f04ce0408e89c839402f83b3f8');
INSERT INTO `sys_dict` VALUES ('285e75fb2c214e14965b8f6a0647d2a1', 'N', '否', 'editable', '否', '10', '93a5e915621c415ba833415d16550043');
INSERT INTO `sys_dict` VALUES ('286369a2800540d38d0a39c72b5d209a', 'VARCHAR', 'VARCHAR', 'jdbcType', 'VARCHAR', '5', '838ebfaff94a42c5abd718e811f4da5f');
INSERT INTO `sys_dict` VALUES ('31e95a234cd34472b48a6d709d84d221', 'BLOCKED', '阻塞', 'quartz_state', '线程阻塞状态', '15', '43c35fe923dd4e80b060f247c780ab69');
INSERT INTO `sys_dict` VALUES ('36962a4c19ea402782a21e427ee8d2f5', 'htmldesign', 'HTML', 'columnCategory', 'UEditor', '40', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('3a1c71bad8384bc58559c3003babf752', 'com_job', '普通任务', 'quartz_category', '普通任务计划', '5', '0d71cae660dd4c76912a946cc83326ba');
INSERT INTO `sys_dict` VALUES ('3ef666c12386464b8bb72919bfedf095', 'asc', '升序', 'sort', '排序方式升序', '1', '006dd9700c6343a996fffdb837d4df02');
INSERT INTO `sys_dict` VALUES ('40140cf333554cfbbb1edc3486392c8e', 'textarea', '多行', 'columnCategory', '多行文本框', '35', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('4088a75a965f4a11acce875fe864623b', 'F', '女', 'sex', '性别', '2', 'f5814ca746a145bc844669a70d984a9c');
INSERT INTO `sys_dict` VALUES ('40e677c9691f42acad06797e3168856f', 'CHS', '汉字', 'valid', '汉字', '8', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('425c2bb98782417aa0e398c3a120504e', '=', '等于', 'query', '排序方式等于', '1', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('43c35fe923dd4e80b060f247c780ab69', 'quartz_state', '任务状态', 'root', '定时任务计划', '9', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('440b144690734530bda8aeff6249647a', 'email', '邮箱', 'valid', '邮箱格式校验', '5', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('44710a4cec6443888a65ba7137991f0c', 'loginName', '帐号', 'valid', '帐号，登录名', '1', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('46d0f15b58d74d0fae3a14d0449c6a33', 'Y', '是', 'editable', '是', '5', '93a5e915621c415ba833415d16550043');
INSERT INTO `sys_dict` VALUES ('47f827e12b1e48938387c9f32744418c', 'idCode', '身份证号', 'valid', '身份证号', '35', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('4f45bf65739c4268b580198001e4265b', 'oracle_operation', 'ORACLE运算', 'root', null, '30', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('4f69dcc49059453ea7d3f24df8dce31a', 'checkbox', '复选', 'columnCategory', '复选框', '25', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('5b613d3461e54e639397ca36ca96348a', 'integer', '正整数', 'valid', '正整数', '40', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('5da5691ce03240b0a2fcaba6b70227fb', 'not like', 'NOT LIKE', 'query', '排序方式NOTLIKE', '30', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('5e24d786442940c7b164fae2d8264821', 'DECIMAL', 'DECIMAL', 'jdbcType', 'DECIMAL', '10', '838ebfaff94a42c5abd718e811f4da5f');
INSERT INTO `sys_dict` VALUES ('6a12255ab2ce4f168e23096040eec120', 'sys_job', '系统任务', 'quartz_category', '系统任务计划', '0', '0d71cae660dd4c76912a946cc83326ba');
INSERT INTO `sys_dict` VALUES ('6ee7ceda158a402985d674361675fc32', 'xiaoshu', '两位小数', 'valid', '两位小数', '44', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('707e7b2585cd468484094fdf94d9e3be', 'CHAR', 'CHAR', 'jdbcType', 'CHAR', '1', '838ebfaff94a42c5abd718e811f4da5f');
INSERT INTO `sys_dict` VALUES ('838ebfaff94a42c5abd718e811f4da5f', 'jdbcType', '数据库字段类型', 'root', '数据库字段类型', '6', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('8473a29b4c7c480cbd4faaf6f8d3726b', 'numberbox', '数值', 'columnCategory', '数字', '10', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('85fa7c92066048f6b312ed5657a3ab91', 'ip', 'IPV4', 'valid', 'IPV4', '17', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('86910beecfa144139c0cb37a44bfe832', 'D', '禁用', 'yes_no_hidden', 'disable', '13', '05da54f04ce0408e89c839402f83b3f8');
INSERT INTO `sys_dict` VALUES ('8bffb44c5100487f93c01c9619448680', 'columnCategory', '字段生成规则', 'root', '字段自动生成', '5', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('8f34b07686d543f98af15332b1b2484e', 'combobox', '列表', 'columnCategory', '下拉框', '15', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('93a5e915621c415ba833415d16550043', 'editable', '是否可编辑', 'root', '是否可编辑', '7', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('96a8c120cb1c4673b7013baeebe3cc19', 'QQ', 'QQ', 'valid', 'QQ', '23', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('9a8e72198a7a48b0a0c1b95395b19ef3', 'N', '否', 'yes_no', '否', '1', '0469a2108e854bbc83d5afa7b3367676');
INSERT INTO `sys_dict` VALUES ('a0ab84edbe73465a97eb8dd70dfff6b0', 'mobile', '手机号', 'valid', '手机号', '26', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('a32de4aa80e747a598070bb030df3abb', 'view_category', '视图分类', 'root', '', '12', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('a5b17303fda549909992510c027eccb7', '>=', '大于等于', 'query', '排序方式大于等于', '20', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('a71be6d72bc9496c97444dd174681255', 'valid', '校验类型', 'root', '表单数据校验类型', '4', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('abec26fffaa142bf8fddbba148cb4fb1', 'desc', '降序', 'sort', '排序方式降序', '5', '006dd9700c6343a996fffdb837d4df02');
INSERT INTO `sys_dict` VALUES ('ad77e71db210421e84a30263bac79fe5', 'sql', 'SQL视图', 'view_category', '', '10', 'a32de4aa80e747a598070bb030df3abb');
INSERT INTO `sys_dict` VALUES ('ae05d12ae8434f15817a9d70f5e77c41', 'money', '金额', 'valid', '钱', '16', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('aec527ed4340470b8b3532fc46886d64', 'oa', '办公流程', 'act_type', null, '5', '046e259674b2451c8790175079479c67');
INSERT INTO `sys_dict` VALUES ('b23f0094a5c54035aac65c3a6e8bbc13', 'query', '查询方式', 'root', '数据库字段查询方式', '3', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('b3a442f4b2b848a2a72a3ae6de3b7306', 'englishOrNum', '英文或数字', 'valid', '英文数字', '38', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('b58d8337fde44cc6931f9a317c1ec461', 'D', '禁用', 'editable', '禁用', '15', '93a5e915621c415ba833415d16550043');
INSERT INTO `sys_dict` VALUES ('b71322b7006842cf8a8085bd23337e45', 'normal', '普通流程', 'act_type', null, '10', '046e259674b2451c8790175079479c67');
INSERT INTO `sys_dict` VALUES ('b9b948f3b6834e0583b2de6e74700e62', 'datebox', '日期', 'columnCategory', '日期', '30', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('ba743fd63cf1437794bd82310ae6000a', 'ZIP', '邮政编码', 'valid', '邮政编码', '20', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('bad1cc7d03c4456ab08347f98297aace', 'N', '否', 'yes_no_hidden', '否', '10', '05da54f04ce0408e89c839402f83b3f8');
INSERT INTO `sys_dict` VALUES ('be8704e7e9c44e4e8eb526a9bfd51f45', 'combotree', '下拉树', 'columnCategory', '下拉树', '38', '8bffb44c5100487f93c01c9619448680');
INSERT INTO `sys_dict` VALUES ('bfc0b1a8cfe143b0a244679851d08a99', 'H', '隐藏', 'editable', '隐藏', '20', '93a5e915621c415ba833415d16550043');
INSERT INTO `sys_dict` VALUES ('c306c0753a51406ea7ae2777899fc71c', '>', '大于', 'query', '排序方式大于', '10', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('cdf35199de2349958465970f01575d3c', 'count', '计数', 'oracle_operation', null, '0', '4f45bf65739c4268b580198001e4265b');
INSERT INTO `sys_dict` VALUES ('d1f5a1db8a8f417782612b0c4a535e05', 'sys', '系统流程', 'act_type', null, '0', '046e259674b2451c8790175079479c67');
INSERT INTO `sys_dict` VALUES ('d2062ed8cf4448e8927b920b8c54e4d5', 'english', '英语', 'valid', '英语', '11', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('d7e56b07528e4d0a940479bf415ebe17', 'integ', '0和正整数', 'valid', '正整数最小为0', '41', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('d9a81aa07d14446d8a2b3aee1db88f80', 'TEXT', 'TEXT', 'jdbcType', 'TEXT', '20', '838ebfaff94a42c5abd718e811f4da5f');
INSERT INTO `sys_dict` VALUES ('dbb54a0a6d844b0ab2bff2f368e065a8', 'mobileAndTel', '手机电话', 'valid', '手机电话', '32', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('dff080312e81483594766b6b5459093e', 'PAUSED', '暂停', 'quartz_state', '暂停状态', '5', '43c35fe923dd4e80b060f247c780ab69');
INSERT INTO `sys_dict` VALUES ('e4f92642033f404db6e46b6a5beeaf9e', 'NORMAL', '正常', 'quartz_state', '正常状态', '0', '43c35fe923dd4e80b060f247c780ab69');
INSERT INTO `sys_dict` VALUES ('e62ca0c84d6b4df3a866c8cc3bad6ecd', 'sum', '求和', 'oracle_operation', null, '5', '4f45bf65739c4268b580198001e4265b');
INSERT INTO `sys_dict` VALUES ('e947732a62164859bc573208bc1c1ea3', 'like', 'LIKE', 'query', '排序方式LIKE', '25', 'b23f0094a5c54035aac65c3a6e8bbc13');
INSERT INTO `sys_dict` VALUES ('eb5a53d4bf35499781ba27ad5ebe9ecc', 'ERROR', '错误', 'quartz_state', '出现错误', '20', '43c35fe923dd4e80b060f247c780ab69');
INSERT INTO `sys_dict` VALUES ('f18d908b8806483ea57ee38c019288ce', 'normal', '普通表单', 'form_category', '', '5', 'f1bb01ea01744f868bacae8f3ea1e78f');
INSERT INTO `sys_dict` VALUES ('f1bb01ea01744f868bacae8f3ea1e78f', 'form_category', '表单分类', 'root', '', '11', '00000000000000000000000000000000');
INSERT INTO `sys_dict` VALUES ('f3511a51bfa347dd9c75518f3870e8c2', 'ChEn', '中英文', 'valid', '中英文', '14', 'a71be6d72bc9496c97444dd174681255');
INSERT INTO `sys_dict` VALUES ('f5814ca746a145bc844669a70d984a9c', 'sex', '性别', 'root', '性别字典', '0', '00000000000000000000000000000000');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `ID` char(32) NOT NULL COMMENT '主键',
  `PARENT_ID` char(32) DEFAULT NULL COMMENT '外键',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `URL` varchar(500) DEFAULT NULL COMMENT 'URL',
  `SEQUENCE` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `ICON` varchar(50) DEFAULT NULL COMMENT '图标',
  `TARGET` varchar(50) DEFAULT NULL COMMENT '打开方式',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='导航菜单';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('00000000000000000000000000000000', 'ffffffffffffffffffffffffffffffff', '导航菜单', '/', '1', '/', null);
INSERT INTO `sys_menu` VALUES ('08e27f9288d04c7a8839dc65f6cbd1fa', 'c617ec2d6eee475ea05fbe779ab3b7c8', '用户管理', '/system/sysUser/list', '10', 'fa fa-user', '_blank');
INSERT INTO `sys_menu` VALUES ('0a0bf010a2e54ee5a82d0d2fe3708d2f', 'c617ec2d6eee475ea05fbe779ab3b7c8', '部门管理', '/system/sysDepartment/list', '8', 'fa fa-sitemap', '_blank');
INSERT INTO `sys_menu` VALUES ('2db887eb47aa458d9d04d54489b0684f', 'c617ec2d6eee475ea05fbe779ab3b7c8', 'DruidMonitor', '/system/druid/index.html', '999', 'fa fa-eye', '_blank');
INSERT INTO `sys_menu` VALUES ('559e0f7fc7d44103814d53dda5e3f197', 'a82bb6d29de94ecfb71202706452e16c', '集群测试', '/bussiness/demo/test', '20', 'fa fa-refresh', '_blank');
INSERT INTO `sys_menu` VALUES ('55e5be06bedc477d8abb445a802d566b', 'a82bb6d29de94ecfb71202706452e16c', 'CRUD演示', '/bussiness/demo/list', '10', 'fa fa-book', '_blank');
INSERT INTO `sys_menu` VALUES ('8bf056fec564462e9f6dd02d89630b9d', 'c617ec2d6eee475ea05fbe779ab3b7c8', '菜单管理', '/system/sysMenu/list', '1', 'fa fa-th-list', '_blank');
INSERT INTO `sys_menu` VALUES ('935acd7355634b278549b0ebbba01876', 'c617ec2d6eee475ea05fbe779ab3b7c8', '角色管理', '/system/sysRole/list', '5', 'fa fa-user-md', '_blank');
INSERT INTO `sys_menu` VALUES ('a82bb6d29de94ecfb71202706452e16c', '00000000000000000000000000000000', '演示DEMO', '/', '10', 'fa fa-heart', '_blank');
INSERT INTO `sys_menu` VALUES ('c617ec2d6eee475ea05fbe779ab3b7c8', '00000000000000000000000000000000', '系统管理', '/', '99', 'fa fa-th', '_blank');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `ID` char(32) NOT NULL COMMENT '主键',
  `NAME` varchar(50) DEFAULT NULL COMMENT '角色名',
  `SHIRO` varchar(50) DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('233e1814d0484ae6ad600a84ae8b2a1b', '员工', 'employee');
INSERT INTO `sys_role` VALUES ('5a0f766da4dd48a0aea154e9b5f736cc', '人事', 'hr');
INSERT INTO `sys_role` VALUES ('bdba2cb597f74e5c83080c82f2e28b49', '部门经理', 'depart');
INSERT INTO `sys_role` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '系统管理员', 'sys');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `ROLE_ID` char(32) NOT NULL COMMENT '角色主键',
  `MENU_ID` char(32) NOT NULL COMMENT '菜单主键',
  PRIMARY KEY (`ROLE_ID`,`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='角色菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '08e27f9288d04c7a8839dc65f6cbd1fa');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '0a0bf010a2e54ee5a82d0d2fe3708d2f');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '2db887eb47aa458d9d04d54489b0684f');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '559e0f7fc7d44103814d53dda5e3f197');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '55e5be06bedc477d8abb445a802d566b');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '8bf056fec564462e9f6dd02d89630b9d');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', '935acd7355634b278549b0ebbba01876');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', 'a82bb6d29de94ecfb71202706452e16c');
INSERT INTO `sys_role_menu` VALUES ('d3f34652eb03447b9cc8bb7375df675d', 'c617ec2d6eee475ea05fbe779ab3b7c8');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` char(32) NOT NULL COMMENT '主键',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT '账号',
  `PASSWORD` varchar(50) DEFAULT NULL COMMENT '密码',
  `NAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `DEPARTMENT_ID` char(32) DEFAULT NULL COMMENT '部门',
  `STATE` char(1) DEFAULT NULL COMMENT '状态',
  `EMAIL` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `PHONE` varchar(50) DEFAULT NULL COMMENT '电话',
  `HEAD_ICON` varchar(100) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='用户信息';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('355222f869db4f4fb8a22e6888aabe48', 'ken', '6efce4fa00c7f982559d05b90207e8b9', '肯', 'e51ba1c688204117bd08b8ce2393e756', '1', null, null, '355222f869db4f4fb8a22e6888aabe48.png');
INSERT INTO `sys_user` VALUES ('3a657ea8ddc745a698d51aeea2183f4d', 'system', 'c4ca86dead4518ac4fd6e30172db3d9e', '管理员', 'dad8faf2f3eb4d279ef04decb91ca4c0', '1', null, null, '3a657ea8ddc745a698d51aeea2183f4d.png');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `USER_ID` char(32) NOT NULL COMMENT '用户主键',
  `ROLE_ID` char(32) NOT NULL COMMENT '角色主键',
  PRIMARY KEY (`USER_ID`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='用户角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('11522a6173804d5ea075ca052c76e20c', '233e1814d0484ae6ad600a84ae8b2a1b');
INSERT INTO `sys_user_role` VALUES ('11522a6173804d5ea075ca052c76e20c', '5a0f766da4dd48a0aea154e9b5f736cc');
INSERT INTO `sys_user_role` VALUES ('355222f869db4f4fb8a22e6888aabe48', '233e1814d0484ae6ad600a84ae8b2a1b');
INSERT INTO `sys_user_role` VALUES ('355222f869db4f4fb8a22e6888aabe48', 'bdba2cb597f74e5c83080c82f2e28b49');
INSERT INTO `sys_user_role` VALUES ('355222f869db4f4fb8a22e6888aabe48', 'd3f34652eb03447b9cc8bb7375df675d');
INSERT INTO `sys_user_role` VALUES ('3a657ea8ddc745a698d51aeea2183f4d', '233e1814d0484ae6ad600a84ae8b2a1b');
INSERT INTO `sys_user_role` VALUES ('3a657ea8ddc745a698d51aeea2183f4d', '5a0f766da4dd48a0aea154e9b5f736cc');
INSERT INTO `sys_user_role` VALUES ('3a657ea8ddc745a698d51aeea2183f4d', 'd3f34652eb03447b9cc8bb7375df675d');
