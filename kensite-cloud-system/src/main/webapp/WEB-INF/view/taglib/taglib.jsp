<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx_zull" value="${pageContext.request.contextPath}"/>
<c:set var="ctx" value="${ctx_zull }/system"/>
<c:set var="ctx_bootstrap" value="${ctx_zull }/resources/bootstrap"/>
<c:set var="ctx_script" value="${ctx_zull }/resources/script"/>
<c:set var="ctx_static" value="${ctx_zull }/resources/static"/>
