# kencloud

#### 介绍

采用Spring Boot + Spring Cloud搭建的分布式微服务开发平台。

#### 技术选型

1、后端

* 核心框架：Spring Boot
* 分布式微服务框架：Spring Cloud Finchley
* 服务注册与发现：Netflix Eureka
* 客户端负载均衡：Netflix Ribbon
* 服务容错保护降级限流：Netflix Hystrix
* 服务路由网关：Netflix Zuul
* 服务端验证：Hibernate Validator 5.1
* 定时任务计划：Quartz
* 持久层框架：MyBatis、MyBatis Plus
* 数据库连接池：Alibaba Druid 1.0
* 缓存框架：Redis
* 日志管理：Log4j
* 工具类：Apache Commons、Jackson 2.2、Xstream 1.4、Dozer 5.3、POI 3.9

2、前端

* JS框架：jQuery 1.9
* CSS框架：Twitter Bootstrap 2.3.1
* 前端组件：easyui
* 客户端验证：JQuery Validation Plugin 1.11
* 富文本：Ueditor
* 对话框：layer
* 上传控件：uploadify
* 树结构控件：jQuery zTree
* 日期控件： My97DatePicker
* 统计图表：echarts

#### 项目介绍

- 基类与工具包：kensite-cloud-base
- 服务治理：kensite-cloud-eureka
- 路由网关：kensite-cloud-zuul
- 门户框架：kensite-cloud-portal
- RBAC基础模块：kensite-cloud-system
- 演示demo：kensite-cloud-bussiness
- 环境工具包：kensite-cloud-env（配置好的Redis和Nginx，解压后双击根目录下startup.bat直接使用）

#### 安装教程

1. 将代码拉取到本地，以maven项目导入到本地开发工具中
2. 创建本地数据库，执行初始化sql脚本。**kensite-cloud-system->resources/db/kencloud.sql**  **kensite-cloud-bussiness->resources/db/kencloud-demo.sql**
3. 修改数据库相关配置。**kensite-cloud-system->resources/application.properties**  **kensite-cloud-bussiness->resources/application.yml**
4. 解压Redis并双击startup.bat启动。
5. 解压Nginx，修改Nginx配置文件（具体参考根目录下《配置文件修改说明.doc》），并双击startup.bat启动。

#### 使用说明

按顺序逐个启动，启动成功后再启动后一个
1. kensite-cloud-eureka -> com.seeyoui.kensite.KensiteCloudEurekaApplication.java
2. kensite-cloud-portal -> com.seeyoui.kensite.KensiteCloudPortalApplication.java
3. kensite-cloud-system -> com.seeyoui.kensite.KensiteCloudSystemApplication.java
4. kensite-cloud-bussiness -> com.seeyoui.kensite.KensiteCloudBusApplication.java
5. kensite-cloud-bussiness -> com.seeyoui.kensite.KensiteCloudBusApplication.java
6. kensite-cloud-zuul -> com.seeyoui.kensite.KensiteCloudZuulApplication.java

说明：bussiness可以只启动一次，启动两次bussiness主要是为了演示集群部署和负载均衡调用，启动第二次时需要将resources/application.yml文件中的端口号修改成一个新的未被占用的端口号。

#### 访问演示

1. 浏览器访问地址：http://localhost/portal/
2. 用户名：system，密码：system
3. 如果集群部署bussiness模块的话，访问菜单：演示DEMO-集群测试，多次刷新此菜单页面会发现，端口号不同，说明集群部署和负载均衡成功实现。
4. 关闭其中一个bussiness，访问菜单：演示DEMO-集群测试，有的会很快，有的会感觉到有一段加载时间，但最终都会返回未关闭的端口号，说明重试机制生效。
5. 其他特性可以自行摸索，也欢迎交流反馈

#### 如何交流、反馈？

* QQ： 2624030701
* QQ群：[102066064](https://jq.qq.com/?_wv=1027&k=475MoAa) （期待你的加入...）
* E-mail：seeyouiken@163.com

![输入图片说明](https://gitee.com/seeyoui/kencloud/raw/master/picture/qq.jpg)

#### 版权声明

本软件使用 [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) 协议，请严格遵照协议内容：

1. 需要给代码的用户一份Apache Licence。
2. 如果你修改了代码，需要在被修改的文件中说明。
3. **在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明。**
4. 如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有Apache Licence。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。
3. Apache Licence也是对商业应用友好的许可。使用者也可以在需要的时候修改代码来满足需要并作为开源或商业产品发布/销售