package com.seeyoui.kensite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class KensiteCloudEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KensiteCloudEurekaApplication.class, args);
	}
}
