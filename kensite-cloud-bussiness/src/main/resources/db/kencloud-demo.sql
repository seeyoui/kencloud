/*
Navicat MySQL Data Transfer

Source Server         : kensite
Source Server Version : 50517
Source Host           : localhost:3306
Source Database       : kencloud

Target Server Type    : MYSQL
Target Server Version : 50517
File Encoding         : 65001

Date: 2019-06-04 15:33:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bo_demo
-- ----------------------------
DROP TABLE IF EXISTS `bo_demo`;
CREATE TABLE `bo_demo` (
  `id` char(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `age` int(10) DEFAULT NULL COMMENT '年龄',
  `brithday` date DEFAULT NULL COMMENT '出生日期',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `hobby` varchar(255) DEFAULT NULL COMMENT '爱好',
  `wages` double(10,2) DEFAULT NULL COMMENT '收入',
  `summary` text COMMENT '简介',
  `is_leave` char(1) DEFAULT NULL COMMENT '是否离职',
  `sex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bo_demo
-- ----------------------------
INSERT INTO `bo_demo` VALUES ('5e2b878c5ea440a39e767b31ba931fe9', '张三', '29', '2019-03-04', '研究生', '足球,篮球,看书', '5120.00', '请多关照', 'F', 'M');
INSERT INTO `bo_demo` VALUES ('d2a4afc2226f484b0068acd99249e75c', '李四', '29', '2019-03-20', '本科', '篮球,看书,游戏', '3900.00', '初来乍到', 'F', 'M');
