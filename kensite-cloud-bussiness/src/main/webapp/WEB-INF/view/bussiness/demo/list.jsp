<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/taglib/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>演示列表</title>
		<%@ include file="/WEB-INF/view/taglib/header.jsp" %>
		<%@ include file="/WEB-INF/view/taglib/easyui.jsp" %>
		<%@ include file="/WEB-INF/view/taglib/layer.jsp" %>
	</head>
	<body>
	 	<div style="position:absolute;top:0px;left:0px;right:0px;bottom:0px;">
			<div style="position:absolute;top:0px;left:0px;right:0px;bottom:0px;">
				<table id="dataList" title="演示列表" 
						class="easyui-datagrid" style="width:100%;height:100%"
						url="${ctx}/demo/list/all"
						toolbar="#toolbar" pagination="true"
						rownumbers="true" fitColumns="false" singleSelect="true">
					<thead>
						<tr>
							<th data-options="field:'id',hidden:true">ID</th>
							<th data-options="field:'ck',checkbox:true"></th>
							<th data-options="field:'name',halign:'center',align:'left',width:100">名称</th>
							<th data-options="field:'age',halign:'center',align:'right',width:100">年龄</th>
							<th data-options="field:'brithday',halign:'center',align:'center',width:150">出生日期</th>
							<th data-options="field:'education',halign:'center',align:'left',width:80">学历</th>
							<th data-options="field:'hobby',halign:'center',align:'left',width:200">爱好</th>
							<th data-options="field:'wages',halign:'center',align:'right',width:80">收入</th>
							<th data-options="field:'yearWages',halign:'center',align:'right',width:80">年收入</th>
							<th data-options="field:'isLeave',halign:'center',align:'center',width:80,
								formatter: function(value,row,index){
									if (value == 'T'){
										return '在职';
									} else {
										return '离职';
									}
								}">是否离职</th>
							<th data-options="field:'summary',halign:'center',align:'left',width:300">简介</th>
						</tr>
					</thead>
				</table>
				<div id="toolbar">
					<div class="toolbar-operate">
						<a href="javascript:void(0)" class="easyui-linkbutton info" iconCls="icon-add" plain="true" onclick="$.demo.newInfo()">新建</a>
						<a href="javascript:void(0)" class="easyui-linkbutton warning" iconCls="icon-edit" plain="true" onclick="$.demo.editInfo()">修改</a>
						<a href="javascript:void(0)" class="easyui-linkbutton error" iconCls="icon-remove" plain="true" onclick="$.demo.destroyInfo()">删除</a>
					</div>
					<div class="toolbar-search">
						<span class="toolbar-title">名称</span>
						<input class="easyui-textbox" id="sel_name" name="sel_name" data-options="width:100"/>
						<span class="toolbar-title">是否离职</span>
						<input class="easyui-combobox" id="sel_isLeave" name="sel_isLeave" data-options="editable:false, icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).combobox('clear');}}],valueField: 'value',textField: 'label',data: [{value: '',label: '全部'},{value: 'T',label: '在职'},{value: 'F',label: '离职'}],panelHeight:'auto',width:100"/>
						<a href="javascript:void(0)" class="easyui-linkbutton success" iconCls="icon-search" plain="true" onclick="$.demo.selectData()">查询</a>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
			});
			var url, loadi;
			var iframeWin = null, iframeBody=null;
			$.demo = {
		   		selectData : function () {
					$('#dataList').datagrid('load',{
						name: $('#sel_name').textbox('getValue'),
						isLeave: $('#sel_isLeave').combobox('getValue'),
					});
				},
				reloadData : function () {
					$.demo.selectData();
				},
				newInfo : function () {
					$('#dataList').datagrid('clearSelections');
					$.demo.layerOpen(url);
				},
				editInfo : function () {
					var row = $('#dataList').datagrid('getSelected');
					if (row) {
						$.demo.layerOpen(url);
					} else {
						layer.msg("请先选择要修改的记录！", {offset: 'rb',icon: 5,shift: 8,time: layerMsgTime});
					}
				},
				layerOpen : function (url) {
					url = '${ctx}/demo/form';
					layer.open({
						type: 2,
						title: '演示基本信息',
						area: ['550px', '550px'],
						fix: false, //不固定
						maxmin: false,
						content: url,
						btn: ['保存', '取消'],
						success: function(layero, index) {
							iframeBody = layer.getChildFrame('body', index);
							iframeWin = window[layero.find('iframe')[0]['name']];
						},
						yes: function(index, layero) {
							if(iframeWin != null) {
								iframeWin.submitInfo();
							}
						},
						cancel: function(index) {
							layer.close(index);
						}
					});
				},
				destroyInfo : function () {
					//var row = $('#dataList').datagrid('getSelected');
					var rows = $('#dataList').datagrid('getSelections');
					if (rows && rows.length>0) {
						layer.confirm('是否确认删除？', {
							btn: ['确定','取消'] //按钮
						}, function() {
							var id = "";
							//id = row.id;
							for(var i=0; i<rows.length; i++) {
								id += rows[i].id+",";
							}
							$.ajax({
								type: "post",
								url: '${ctx}/demo/delete',
								data: {id:id},
								dataType: 'json',
								timeout: layerLoadMaxTime,
								beforeSend: function(XMLHttpRequest) {
									loadi = layer.load(2, {shade: layerLoadShade,time: layerLoadMaxTime});
								},
								success: function(data, textStatus) {
									layer.close(loadi);
									if (data.success==TRUE) {
										layer.msg("操作成功！", {offset: 'rb',icon: 6,shift: 8,time: layerMsgTime});
										$.demo.reloadData();
									} else {
										layer.msg("操作失败！", {offset: 'rb',icon: 5,shift: 8,time: layerMsgTime});
									}
								},
								error: function(request, errType) {
									layer.close(loadi);
									//"timeout", "error", "notmodified" 和 "parsererror"
									if(errType == 'timeout') {
										layer.msg('请求超时', {offset: 'rb',icon: 6,shift: 8,time: layerMsgTime});
									}
									if(errType == 'error') {
										layer.msg('系统错误，请联系管理员', {offset: 'rb',icon: 6,shift: 8,time: layerMsgTime});
									}
								}
							});
						}, function() {
						});
					} else {
						layer.msg("请先选择要删除的记录！", {offset: 'rb',icon: 5,shift: 8,time: layerMsgTime});
					}
				}
			}
		</script>
	</body>
</html>
