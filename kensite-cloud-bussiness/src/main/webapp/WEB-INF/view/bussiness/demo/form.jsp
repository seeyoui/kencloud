<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/taglib/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>演示</title>
		<%@ include file="/WEB-INF/view/taglib/header.jsp" %>
		<%@ include file="/WEB-INF/view/taglib/easyui.jsp" %>
		<%@ include file="/WEB-INF/view/taglib/layer.jsp" %>
	</head>
	<body>
	 	<div style="position:absolute;top:10px;left:20px;right:20px;bottom:10px;">
			<form id="dataForm" method="post">
				<div class="fitem">
					<label>名称</label>
					<input class="easyui-textbox" id="name" name="name" data-options="tipPosition:'bottom', required:true"/>
					<label>年龄</label>
					<input class="easyui-numberbox" id="age" name="age" data-options="tipPosition:'bottom', required:true, min:0, precision:0, value:0"/>
				</div>
				<div class="fitem">
					<label>出生日期</label>
					<input class="date-input easyui-validatebox" id="brithday" name="brithday" data-options="tipPosition:'bottom', readonly:true"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"/>
					<label>月收入</label>
					<input class="easyui-numberbox" id="wages" name="wages" data-options="tipPosition:'bottom', required:true, min:0, precision:2, value:0"/>
				</div>
				<div class="fitem">
					<label>学历</label>
					<input class="easyui-radiobutton" id="education1" name="education" data-options="tipPosition:'bottom',required:true,label:'博士生',value:'博士生',labelPosition:'after',labelWidth:50,checked:true"/>
					<input class="easyui-radiobutton" id="education2" name="education" data-options="tipPosition:'bottom',required:true,label:'研究生',value:'研究生',labelPosition:'after',labelWidth:50"/>
					<input class="easyui-radiobutton" id="education3" name="education" data-options="tipPosition:'bottom',required:true,label:'本科',value:'本科',labelPosition:'after',labelWidth:50"/>
					<input class="easyui-radiobutton" id="education4" name="education" data-options="tipPosition:'bottom',required:true,label:'专科',value:'专科',labelPosition:'after',labelWidth:50"/>
					<input class="easyui-radiobutton" id="education5" name="education" data-options="tipPosition:'bottom',required:true,label:'职高',value:'职高',labelPosition:'after',labelWidth:50"/>
				</div>
				<div class="fitem">
					<label>爱好</label>
					<input class="easyui-checkbox" id="hobby足球" name="hobby" data-options="tipPosition:'bottom',required:true,label:'足球',value:'足球',labelPosition:'after',labelWidth:50,checked:true"/>
					<input class="easyui-checkbox" id="hobby篮球" name="hobby" data-options="tipPosition:'bottom',required:true,label:'篮球',value:'篮球',labelPosition:'after',labelWidth:50"/>
					<input class="easyui-checkbox" id="hobby跑步" name="hobby" data-options="tipPosition:'bottom',required:true,label:'跑步',value:'跑步',labelPosition:'after',labelWidth:50"/>
					<input class="easyui-checkbox" id="hobby看书" name="hobby" data-options="tipPosition:'bottom',required:true,label:'看书',value:'看书',labelPosition:'after',labelWidth:50"/>
					<input class="easyui-checkbox" id="hobby游戏" name="hobby" data-options="tipPosition:'bottom',required:true,label:'游戏',value:'游戏',labelPosition:'after',labelWidth:50"/>
				</div>
				<div class="fitem">
					<label>简介</label>
					<input class="easyui-textbox" id="summary" name="summary" data-options="tipPosition:'bottom', multiline:true, height:160, width:405, prompt: '简单介绍一下自己'"/>
				</div>
				<div class="fitem">
					<label>是否离职</label>
					<input class="easyui-combobox" id="isLeave" name="isLeave" data-options="editable:false, icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).combobox('clear');}}],valueField: 'value',textField: 'label',data: [{value: 'T',label: '是'},{value: 'F',label: '否'}],panelHeight:'auto',value:'T'"/>
				</div>
				<input id="id" name="id" type="hidden"/>
			</form>
		</div>
		<script type="text/javascript">
			var loadi,url,index = parent.layer.getFrameIndex(window.name);
			$(document).ready(function() {
				var row = parent.$('#dataList').datagrid('getSelected');
				url = '${ctx}/demo/save';
				if(row != null) {
					$('#dataForm').form('load', row);
					var hobby = row.hobby;
					if(hobby) {
						var hobbyArr = hobby.split(',');
						for(var i=0; i<hobbyArr.length; i++) {
							$('#hobby'+hobbyArr[i]).checkbox('check');
						}
					}
					url = '${ctx}/demo/update';
				}
			});
			
			function submitInfo() {
				$('#dataForm').form('submit',{
					url: url,
					onSubmit: function(param) {
						if($(this).form('validate')) {
							loadi = parent.layer.load(2, {shade: layerLoadShade,time: layerLoadMaxTime});
							return true;
						} else {
							return false;
						}
					},
					success: function(data) {
						parent.layer.close(loadi);
						cleanErrMsg();
						var data = eval('(' + data + ')');
						if (data.success==TRUE) {
							parent.$.demo.reloadData();
							parent.layer.msg("操作成功！", {offset: layerMsgOffset,icon: 6,shift: 8,time: layerMsgTime});
							parent.layer.close(index);
						} else {
							renderErrMsg(data.message);
						}
					}
				});
			}
		</script>
	</body>
</html>
