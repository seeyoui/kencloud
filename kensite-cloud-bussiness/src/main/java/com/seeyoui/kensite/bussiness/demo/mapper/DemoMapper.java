package com.seeyoui.kensite.bussiness.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seeyoui.kensite.bussiness.demo.domain.Demo;

public interface DemoMapper extends BaseMapper<Demo> {
}
