package com.seeyoui.kensite.bussiness.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seeyoui.kensite.bussiness.demo.domain.Demo;
import com.seeyoui.kensite.bussiness.demo.mapper.DemoMapper;

@Service
public class DemoService extends ServiceImpl<DemoMapper, Demo> {

	@Autowired
	private DemoMapper demoMapper;
	
	public void selectAll() {
		demoMapper.selectList(null);
	}
}
