package com.seeyoui.kensite.bussiness.demo.domain;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

@TableName("bo_demo")
public class Demo {

	@TableId(value="id",type=IdType.UUID)
	private String id;// 主键UUID
	@TableField(value="name")
	private String name;// 姓名，前端是单行输入框
	@TableField(value="age")
	private int age;// 年龄，前端是整数数字框
	@TableField(value="brithday")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date brithday;// 出生日期，前端是日期选择框
	@TableField(value="education")
	private String education;// 学历，前端是单选框
	@TableField(value="hobby")
	private String hobby;// 爱好，前端是复选框
	@TableField(value="wages")
	private double wages;// 收入，前端是复数数值框
	@TableField(value="yearWages",exist=false)
	private double yearWages;// 年收入，数据库没有这个字段，通过wages*12自动算出
	@TableField(value="summary")
	private String summary;// 简介，前端是多行编辑框，数据库对应大文本存储
	@TableField(value="is_leave")
	private String isLeave;// 是否离职，前端是选择按钮
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getBrithday() {
		return brithday;
	}
	public void setBrithday(Date brithday) {
		this.brithday = brithday;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public double getWages() {
		return wages;
	}
	public void setWages(double wages) {
		this.wages = wages;
	}
	public double getYearWages() {
		return 12 * this.wages;
	}
	public void setYearWages(double yearWages) {
		this.yearWages = yearWages;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getIsLeave() {
		return isLeave;
	}
	public void setIsLeave(String isLeave) {
		this.isLeave = isLeave;
	}
	
}
