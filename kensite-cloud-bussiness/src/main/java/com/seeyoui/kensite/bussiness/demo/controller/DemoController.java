package com.seeyoui.kensite.bussiness.demo.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.seeyoui.kensite.base.controller.BaseController;
import com.seeyoui.kensite.bussiness.demo.domain.Demo;
import com.seeyoui.kensite.bussiness.demo.service.DemoService;

@Controller
@RequestMapping(value="/demo")
public class DemoController extends BaseController<Demo> {
	
	@Autowired
	private DemoService demoService;

	@RequestMapping(value = "/hello")
	@ResponseBody
	public Object hello(HttpSession session, HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap) throws Exception {
		return "bussiness"+request.getServerPort();
	}
	
	/**
	 * 展示列表页面
	 * 
	 * @param modelMap
	 * @param module
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{page}")
	public ModelAndView showSysDepartmentPageList(HttpSession session, HttpServletResponse response,
			HttpServletRequest request, ModelMap modelMap, @PathVariable String page) throws Exception {
		int port = request.getServerPort();
		System.err.println("调用页面成功====当前服务端口为："+port);
		modelMap.put("port", port);
		return new ModelAndView("/bussiness/demo/" + page, modelMap);
	}

	/**
	 * 获取表单数据
	 * @param session
	 * @param response
	 * @param request
	 * @param modelMap
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/form/data", method = RequestMethod.POST)
	@ResponseBody
	public Object formData(HttpSession session, HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, String id) throws Exception {
		Demo demo = demoService.getById(id);
		return demo;
	}

	/**
	 * 获取列表分页数据
	 * @param session
	 * @param response
	 * @param request
	 * @param demo
	 * @param current
	 * @param size
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list/data")
	@ResponseBody
	public Object listData(HttpSession session, HttpServletResponse response, HttpServletRequest request,
			Demo demo, @RequestParam("page") Integer current, @RequestParam("rows") Integer size) throws Exception {
		Page<Demo> page = new Page<Demo>(current, size);
		QueryWrapper<Demo> queryWrapper = new QueryWrapper<>();
		buildQueryWrapper(demo, queryWrapper);
		IPage<Demo> demoList = demoService.page(page, queryWrapper);
		return renderDataGrid(demoList.getRecords(), demoList.getTotal());
	}

	/**
	 * 获取列表所有数据
	 * @param session
	 * @param response
	 * @param request
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list/all")
	@ResponseBody
	public Object listAll(HttpSession session, HttpServletResponse response, HttpServletRequest request,
			Demo demo) throws Exception {
		QueryWrapper<Demo> queryWrapper = new QueryWrapper<>();
		buildQueryWrapper(demo, queryWrapper);
		List<Demo> demoList = demoService.list(queryWrapper);
		return demoList;
	}
	
	/**
	 * 保存新增的数据
	 * 
	 * @param modelMap
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public Object save(HttpSession session, HttpServletResponse response, HttpServletRequest request, ModelMap modelMap,
			Demo demo) throws Exception {
		boolean success = demoService.save(demo);
		if(success) {
			return renderSuccess();
		} else {
			return renderError("保存失败！");
		}
	}
	
	/**
	 * 保存修改的数据
	 * 
	 * @param modelMap
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public Object update(HttpSession session, HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, Demo demo) throws Exception {
		boolean success = demoService.updateById(demo);
		if(success) {
			return renderSuccess();
		} else {
			return renderError("修改失败！");
		}
	}
	
	/**
	 * 删除数据
	 * 
	 * @param modelMap
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Object delete(HttpSession session, HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, String id) throws Exception {
		List<String> listId = Arrays.asList(id.split(","));
		boolean success = demoService.removeByIds(listId);
		if(success) {
			return renderSuccess();
		} else {
			return renderError("删除失败！");
		}
	}
	
	/**
	 * 构建查询器
	 * @param demo
	 * @param queryWrapper
	 */
	private static void buildQueryWrapper(Demo demo, QueryWrapper<Demo> queryWrapper) {
		if(demo.getName() != null && !"".equals(demo.getName())) {
			queryWrapper.like("name", demo.getName());
		}
		if(demo.getIsLeave() != null && !"".equals(demo.getIsLeave())) {
			queryWrapper.eq("is_leave", demo.getIsLeave());
		}
	}
}
