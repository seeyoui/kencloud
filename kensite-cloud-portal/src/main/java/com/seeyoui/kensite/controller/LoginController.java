package com.seeyoui.kensite.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.seeyoui.kensite.remote.ZuulRemote;

@Controller
public class LoginController {
	
	@Autowired
    ZuulRemote zuulRemote;
	
	@RequestMapping(value = "/")
	public ModelAndView page(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap) throws Exception {
		return new ModelAndView("index", modelMap);
	}
	
	@RequestMapping(value = "/home")
	public ModelAndView home(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap) throws Exception {
		return new ModelAndView("home", modelMap);
	}
	
	@RequestMapping(value = "/login")
    public Object login(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap, String userName, String passWord) {
		Map<String, Object> result = zuulRemote.login(userName, passWord);
		if((boolean) result.get("success")) {
			System.err.println("portal      sessionId=" + request.getSession().getId());
			request.getSession().setAttribute("currentUserName", result.get("currentUserName"));
			request.getSession().setAttribute("currentUser", result.get("currentUser"));
			request.getSession().setAttribute("menuList", result.get("menuList"));
			return new ModelAndView("main", modelMap);
		} else {
			modelMap.put("message", result.get("message"));
			return new ModelAndView("index", modelMap);
		}
	}
	
	@RequestMapping(value = "/logout")
    public Object logout(HttpSession session,
			HttpServletResponse response, HttpServletRequest request,
			ModelMap modelMap) {
		Map<String, Object> result = zuulRemote.logout();
		if(!(boolean) result.get("success")) {
			modelMap.put("message", result.get("message"));
		}
		return new ModelAndView("index", modelMap);
	}
}