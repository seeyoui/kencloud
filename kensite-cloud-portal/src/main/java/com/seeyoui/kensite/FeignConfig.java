package com.seeyoui.kensite;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;

import feign.RequestInterceptor;

@Configuration
public class FeignConfig {

	@Bean
	public RequestInterceptor requestInterceptor() {
		return requestTemplate -> {
			String sessionId = RequestContextHolder.currentRequestAttributes()
					.getSessionId();
			if (sessionId != null && !"".equals(sessionId)) {
				requestTemplate.header("Cookie", "SESSION=" + sessionId);
			}
		};
	}
}
