package com.seeyoui.kensite.remote;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class ZuulRemoteHystrix implements ZuulRemote{

    @Override
    public Map<String, Object> login(@RequestParam(value = "userName") String userName, @RequestParam(value = "passWord")String passWord) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("success", false);
    	result.put("message", "服务器异常");
        return result;
    }
    
    @Override
    public Map<String, Object> logout() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("success", false);
    	result.put("message", "服务器异常");
        return result;
    }
}
