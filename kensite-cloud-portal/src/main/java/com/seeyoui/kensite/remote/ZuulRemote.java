package com.seeyoui.kensite.remote;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name= "kensite-cloud-system", fallback = ZuulRemoteHystrix.class)
public interface ZuulRemote {

    @RequestMapping(value = "/sysLogin/login", method=RequestMethod.POST)
    public Map<String, Object> login(@RequestParam(value = "userName") String userName, @RequestParam(value = "passWord")String passWord);
    
    @RequestMapping(value = "/sysLogin/logout", method=RequestMethod.POST)
    public Map<String, Object> logout();

}
