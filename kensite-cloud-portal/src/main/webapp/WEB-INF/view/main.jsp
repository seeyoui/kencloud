<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/taglib/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kensite</title>
<%@ include file="/WEB-INF/view/taglib/header.jsp"%>
<%@ include file="/WEB-INF/view/taglib/layer.jsp" %>
<%@ include file="/WEB-INF/view/taglib/bootstrap.jsp"%>
<style type="text/css">
.content img {
	width:32px;
	height:32px;
}
</style>
</head>
<body class="fixed-sidebar full-height-layout gray-bg"
	style="overflow: hidden">
	<div id="wrapper">
		<!--左侧导航开始-->
		<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="nav-close">
			<i class="fa fa-times-circle"></i>
		</div>
		<div class="sidebar-collapse">
			<ul class="nav" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span>
							<c:if test="${not empty currentUser.headIcon}">
								<img id="headIcon" alt="image" class="img-circle" src="${ctx_resources}/upload/headIcon/${currentUser.headIcon}" style="width: 60px;"/>
							</c:if>
							<c:if test="${empty currentUser.headIcon}">
								<img id="headIcon" alt="image" class="img-circle" src="${ctx_resources}/upload/headerIcon.png" style="width: 60px;"/>
							</c:if>
							
						</span>
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<span class="clear" style="position: absolute; left: 80px;top: 10px;">
							<span class="block m-t-xs"><strong class="font-bold">${currentUserName}</strong></span>
							<span class="text-muted text-xs block">${currentUser.name}<b class="caret"></b></span>
							</span>
						</a>
						<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<!-- <li><a href="javascript:headIcon();">修改头像</a></li> -->
							<li><a class="J_menuItem" href="${ctx }/login/skinsPage/common/headIcon">修改头像</a></li>
							<!-- <li><a class="J_menuItem" href="profile.html">个人资料</a></li> -->
							<li><a href="javascript:updatePassword()">修改密码</a></li>
							<li class="divider"></li>
							<li><a href="javascript:logout();">安全退出</a></li>
						</ul>
					</div>
					<div class="logo-element">Dap</div>
				</li>
				<!-- 生成菜单导航 -->
				<c:forEach var="tree" items="${menuList}" varStatus="status">
				<li>
					<c:if test="${tree.attributes.url=='/'}">
					<a href="javasctipt:void(0)">
					</c:if>
					<c:if test="${tree.attributes.url!='/'}">
					<a class="J_menuItem" href="${ctx_zuul }${tree.attributes.url}">
					</c:if>
						<c:if test="${not empty tree.attributes.icon}">
						<i class="${tree.attributes.icon}"></i>
						</c:if>
						<span class="nav-label">${tree.text}</span>
						<c:if test="${not empty tree.children}">
						<span class="fa arrow"></span>
						</c:if>
					</a>
					<c:if test="${tree.children!=null}">
					<ul class="nav nav-second-level">
					<c:forEach var="tree1" items="${tree.children}" varStatus="status">
						<li>
						<c:if test="${tree1.attributes.url=='/'}">
						<a href="javasctipt:void(0)">
						</c:if>
						<c:if test="${tree1.attributes.url!='/'}">
						<a class="J_menuItem" href="${ctx_zuul }${tree1.attributes.url}">
						</c:if>
						<c:if test="${not empty tree1.attributes.icon}">
						<i class="${tree1.attributes.icon}"></i>
						</c:if>
						${tree1.text}
						<c:if test="${not empty tree1.children}">
						<span class="fa arrow"></span>
						</c:if>
						</a>
						<c:if test="${not empty tree1.children}">
						<ul class="nav nav-third-level">
						<c:forEach var="tree2" items="${tree1.children}" varStatus="status">
                            <li><c:if test="${tree2.attributes.url=='/'}">
							<a href="javasctipt:void(0)">
							</c:if>
							<c:if test="${tree2.attributes.url!='/'}">
							<a class="J_menuItem" href="${ctx_zuul }${tree2.attributes.url}">
							</c:if>
							<c:if test="${not empty tree2.attributes.icon}">
							<i class="${tree2.attributes.icon}"></i>
							</c:if>
                            ${tree2.text}</a>
                            </li>
						</c:forEach>
						</ul>
						</c:if>
						</li>
					</c:forEach>
					</ul>
					</c:if>
				</li>
				</c:forEach>
			</ul>
		</div>
		</nav>
		<!--左侧导航结束-->
		<!--右侧部分开始-->
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li class="hidden-xs">
							<a href="javascript:logout();">
								<i class="fa fa fa-sign-out"></i>退出
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="row content-tabs">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" style="position: absolute; left: -19px;top: -5px;">
					<i class="fa fa-bars"></i>
				</a>
				<button class="roll-nav roll-left J_tabLeft">
					<i class="fa fa-backward"></i>
				</button>
				<nav class="page-tabs J_menuTabs">
					<div class="page-tabs-content">
						<a href="javascript:;" class="active J_menuTab" data-id="${ctx }/home">首页</a>
					</div>
				</nav>
				<button class="roll-nav roll-right J_tabRight">
					<i class="fa fa-forward"></i>
				</button>
				<div class="btn-group roll-nav roll-right">
					<button class="dropdown J_tabClose" data-toggle="dropdown">
						关闭操作<span class="caret"></span>
					</button>
					<ul role="menu" class="dropdown-menu dropdown-menu-right">
						<li class="J_tabShowActive"><a>定位当前选项卡</a></li>
						<li class="divider"></li>
						<li class="J_tabCloseAll"><a>关闭全部选项卡</a></li>
						<li class="J_tabCloseOther"><a>关闭其他选项卡</a></li>
					</ul>
				</div>
			</div>
			<div class="row J_mainContent" id="content-main">
				<iframe class="J_iframe" name="iframe0" width="100%" height="100%"
					src="${ctx }/home" frameborder="0" data-id="${ctx }/home" seamless></iframe>
			</div>
			<div class="footer">
				<div class="pull-right">
					&copy; 2014-2018
				</div>
			</div>
		</div>
		<!--右侧部分结束-->
	</div>
</body>
<script type="text/javascript">
$(document).ready(function() {
});
//退出当前登录
function logout() {
	layer.confirm('你确定要退出系统么？',function(index){
		window.location.href="${ctx}/logout";
	});
}

function updatePassword() {
	var url = "${ctx }/login/skinsPage/common/updatePassword";
	var title = "修改密码";
	var area = ['350px', '450px'];
	layerOpen(url, title, area);
}

function layerOpen(url, title, area) {
	if(area == null) {
		area = ['310px', '350px'];
	}
   	layer.open({
   	    type: 2,
   	    title: title,
   	    area: area,
   	    fix: false, //不固定
   	    maxmin: false,
   	    content: url,
   	    btn: ['保存', '取消'],
           success: function(layero, index){
               iframeBody = layer.getChildFrame('body', index);
               iframeWin = window[layero.find('iframe')[0]['name']];
           },
   	    yes: function(index, layero) {
   	    	if(iframeWin != null) {
   	    		iframeWin.submitInfo();
   	    	}
   	    },
   	    cancel: function(index){
   	    	layer.close(index);
   	    }
   	});
}
</script>
</html>