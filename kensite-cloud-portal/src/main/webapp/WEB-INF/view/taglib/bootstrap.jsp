<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	
	<!--[if lt IE 8]>
	<meta http-equiv="refresh" content="0;ie.html" />
	<![endif]-->
	<link href="${ctx_bootstrap}/css/bootstrap.min.css" rel="stylesheet">
	<link href="${ctx_bootstrap}/css/font-awesome.min.css" rel="stylesheet">
	<link href="${ctx_bootstrap}/css/animate.min.css" rel="stylesheet">
	<script src="${ctx_bootstrap}/js/bootstrap.min.js"></script>
	<script src="${ctx_bootstrap}/js/metisMenu/jquery.metisMenu.js"></script>
	<script src="${ctx_bootstrap}/js/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${ctx_bootstrap}/js/pace/pace.min.js"></script>
	<link href="${ctx_static}/skins/bootstrap/css/style.min.css" rel="stylesheet">
	<script src="${ctx_static}/skins/bootstrap/js/hplus.min.js"></script>
	<script src="${ctx_static}/skins/bootstrap/js/contabs.min.js"></script>
