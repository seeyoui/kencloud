<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/taglib/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kensite Cloud</title>
<%@ include file="/WEB-INF/view/taglib/header.jsp"%>
<%@ include file="/WEB-INF/view/taglib/layer.jsp" %>
<%@ include file="/WEB-INF/view/taglib/bootstrap.jsp"%>
<link href="${ctx_static}/skins/bootstrap/css/login.min.css" rel="stylesheet">
<style type="text/css">
.layui-layer-content {
	color: #000;
}
</style>
</head>
<body class="signin">
    <div class="signinpanel">
        <div class="row">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-6">
                <form class="form-1" method="post" id="loginForm" name="loginForm" action="${ctx}/login">
                    <h4 class="no-margins">kensite Cloud</h4>
                    <p class="m-t-md">欢迎登陆</p>
                    <input id="userName" name="userName" type="text" class="form-control uname" placeholder="帐号" />
                    <input id="passWord" name="passWord" type="password" class="form-control pword m-b" placeholder="密码" />
                    <a href="${ctx}/skins/main">忘记密码了？</a>
                    <a href="javascript:void(0);" class="btn btn-success btn-block submit">登录</a>
                </form>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
         <div class="row">
            <div class="col-sm-2">
            </div>
            <div class="col-sm-8">
            	<div class="signup-footer">
		            <div class="pull-left">
		                <a href="http://www.seeyoui.com">seeyoui</a>&nbsp;&nbsp;&nbsp;&nbsp;&copy; 2018 All Rights Reserved.
		            </div>
		        </div>
            </div>
            <div class="col-sm-2">
            </div>
         </div>
    </div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('.submit').click(function() {
			$('#loginForm').submit();
		});
		//添加“回车”事件
		$(document).keydown(function(e) {
			if (e.keyCode === 13) {
				$('.submit').click();
			}
		});
		<c:if test="${not empty message}">
			layer.msg('${message}', {
			    offset: 'rb',
				icon: 5,
			    shift: 8,
			    time: 200000
			});
		</c:if>
	});
</script>
</html>