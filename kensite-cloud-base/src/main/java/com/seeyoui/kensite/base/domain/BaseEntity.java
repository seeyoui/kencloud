package com.seeyoui.kensite.base.domain;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 数据Entity类
 * @author SeeYoui
 * @version 2014-05-16
 */
@MappedSuperclass
public abstract class BaseEntity<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	protected int page;//当前页号
	protected int rows;//每页行数
	protected int row;//当前行号
	protected String sort; //排序字段
	protected String order; //排序方式
	
	@JsonIgnore
	public int getPage() {
		if(this.page == 0) {
			return 1;
		}
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@JsonIgnore
	public int getRows() {
		if(this.rows == 0) {
			return 20;
		}
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	@JsonIgnore
	public int getRow() {
		if((this.page-1)*this.rows < 0) {
			return 0;
		}
		return (this.page-1)*this.rows;
	}

	public void setRow(int row) {
		this.row = row;
	}

	@JsonIgnore
	public String getSort() {
		return this.sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@JsonIgnore
	public String getOrder() {
		return this.order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

}
