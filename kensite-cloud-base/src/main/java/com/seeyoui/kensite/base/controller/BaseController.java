package com.seeyoui.kensite.base.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

import com.seeyoui.kensite.base.validator.BeanValidators;

/**
 * 控制器支持类
 * @author ken
 * @version 2015-03-25
 */
public abstract class BaseController<T> {

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 验证Bean实例对象
	 */
	@Autowired
	protected Validator validator;

	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组
	 * @return 验证成功：返回true；严重失败：将错误信息添加到 message 中
	 */
	protected boolean beanValidator(ModelMap model, Object object, Class<?>... groups) {
		try{
			BeanValidators.validateWithException(validator, object, groups);
		}catch(ConstraintViolationException ex){
			Map<String, String> map = BeanValidators.extractPropertyAndMessage(ex);
			model.addAttribute("message", map);
			return false;
		}
		return true;
	}
	
	protected Map<String, Object> renderDataGrid(List<T> rows) {
		return renderDataGrid(rows, rows.size());
	}
	
	protected Map<String, Object> renderDataGrid(List<T> rows, long total) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", rows);
		result.put("total", total);
		return result;
	}
	
	protected Map<String, Object> renderSuccess() {
		return renderSuccess("操作成功！");
	}
	
	protected Map<String, Object> renderSuccess(String message) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", "T");
		result.put("message", message);
		return result;
	}
	
	protected Map<String, Object> renderError() {
		return renderError("操作失败！");
	}
	
	protected Map<String, Object> renderError(String message) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", "F");
		result.put("message", message);
		return result;
	}
}
